const TerserPlugin = require("terser-webpack-plugin")

module.exports = () =>  new TerserPlugin({ sourceMap: true, extractComments: true })
