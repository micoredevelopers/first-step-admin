import $ from 'jquery'

$('#group-select').on('change', function () {
  const value = $(this).val()

  if (value !== '0') {
    $('.btn-select-group').attr('disabled', false)
  }
})
